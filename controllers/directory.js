'use strict';
falconApp_directory.config(['$routeProvider', function($routeProvider) {
  $routeProvider
	.when('/directories', {
		templateUrl: 'views/directory/index.html',
		controller: 'index'
	})
	.when('/directories/upload', {
		templateUrl: 'views/directory/upload.html',
		controller: 'upload'
	})
	.when('/', {
		templateUrl: 'views/directory/index.html',
		controller: 'index'
	})
	.otherwise({redirectTo: '/'});
}]);

falconApp_directory.controller('index', ['$scope', '$http', function($scope,$http) {
	$scope.errors = [];

    $http.get(serviceBase + 'directories').success(function(data){
    	$scope.directories = data;
    });

    $scope.serviceBase = serviceBase;

	$scope.addDirectory = function(){
		$scope.errors = [];
		if ($scope.directory && $scope.directory.id){
			$http.put( serviceBase + 'directories/' + $scope.directory.id, $scope.directory )
				.success(function(data){
					$scope.directory = {};
					$http.get(serviceBase + 'directories').success(function(data){
				    	$scope.directories = data;
				    });
				})
				.error(function(result){
					$scope.errors = result;
				});
		}else{
			$http.post( serviceBase + 'directories', $scope.directory )
				.success(function(data){
					$scope.directory = {};
					$http.get(serviceBase + 'directories').success(function(data){
				    	$scope.directories = data;
				    });
				})
				.error(function(result){
					$scope.errors = result;
				});
		}
	}

	$scope.editDirectory = function(item){
		$scope.errors = [];
		$scope.directory = {shop: item.shop, location: item.location, id: item.id};
	}

	$scope.deleteDirectory = function(item){
		$scope.errors = [];
		if (item && item.id){
			if (confirm('Are you sure you want to delete this?')) {
				$http.delete( serviceBase + 'directories/' + item.id )
					.success(function(data){
						$scope.directory = {};
						$http.get(serviceBase + 'directories').success(function(data){
					    	$scope.directories = data;
					    });
					})
					.error(function(result){
						$scope.errors = result;
					});
			}
		}
	}
}])

.controller('upload', ['$scope', '$http', 'FileUploader', function($scope,$http,FileUploader) {
	$scope.errors = [];

	$http.get(serviceBase + 'directories').success(function(data){
    	$scope.directories = data;
    });

	$scope.uploadDirectory = function(){
		if ($scope.uploader.queue.length){
			$scope.uploadItem = $scope.uploader.queue[$scope.uploader.queue.length - 1];
			$scope.uploadItem.upload();
		}else{
			alert("Please select a text file with a .txt extension");
		}
	}

	var uploader = $scope.uploader = new FileUploader({
        url: serviceBase + 'directories/upload'
    });

    uploader.filters.push({
	    name: 'txtFilter',
	    fn: function(item) {
	        return item.type == 'text/plain';
	    }
	});

    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $scope.errors = [];
        $scope.uploader.clearQueue();
        document.getElementById('fileupload').value = null;
        if (!response.status){
        	$scope.errors = response.message;
        }else{
        	$scope.success = true;
        }

        $http.get(serviceBase + 'directories').success(function(data){
	    	$scope.directories = data;
	    });
    };

    $scope.clearProgressBar = function(){
    	$scope.uploadItem = null;
    	$scope.success = false;
    };

}]);