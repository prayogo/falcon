'use strict';
var serviceBase = '/falcon/services/web/'
// Declare app level module which depends on views, and components
var falconApp = angular.module('falconApp', [
  'ngRoute',
  'falconApp.directory',
  'angularFileUpload',
]);
var falconApp_directory = angular.module('falconApp.directory', ['ngRoute']);

falconApp.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/'});
}]);