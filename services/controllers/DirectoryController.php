<?php

namespace app\controllers;

use yii\rest\ActiveController;

class DirectoryController extends ActiveController
{
	public $modelClass = 'app\models\Directory';

    public function actionDownload(){
    	
		header("Content-type: text/plain");
   		header("Content-Disposition: attachment; filename=directories.txt");

   		$directories = \app\models\Directory::find()->orderBy('shop')->all();
   		if ( count($directories) <= 0 ){
   			echo "No directory";
   		}else{
	   		echo "Shop Name" . "\t" . "Location" . PHP_EOL;
   			foreach ($directories as $key => $value) {
	   			echo $value->shop . "\t" . $value->location . PHP_EOL;
	   		}
   		}

    }

    public function actionUpload(){

    	$result = [
    		"status" => false,
    		"message" => []
    	];

        if ( !empty( $_FILES ) ) {
        	//Read file
        	$file = fopen( $_FILES[ 'file' ][ 'tmp_name' ], "r" );

        	$text = "";
			while( $data = fread( $file, 1024 ) ){
				$text = $data;
			}

			//Split text by lines
			$lines = explode( PHP_EOL, trim($text) );
			if (count($lines) <= 0){
				$error = ["line" => "Error on line 0", "errors" => "No directory uploaded"];
				array_push($result["message"], $error);
			}else{
				
				$connection = \Yii::$app->db;
	            $transaction = $connection->beginTransaction(); 
				
				$count = 0;
				$id = [];
				foreach ($lines as $directory) {
					//Split lines text by tab seperator
					//First column is Shop
					//Second column is Location
					$count++;
					$data = explode( "\t", trim($directory) );
					$shop = isset($data[0]) ? $data[0] : '';
					$location = isset($data[1]) ? $data[1] : '';

					if ($shop == "Shop Name" && $location == "Location"){
						continue;
					}

					//Update directory by location, because my assumption location is unique, while shop not.
					$model = \app\models\Directory::find()->where(['location' => $location])->one();
					if ($model != null){
						//Update
						$model->shop = $shop;
						$model->location = $location;
					}else{
						//Insert
						$model = new \app\models\Directory();
						$model->shop = $shop;
						$model->location = $location;
					}
					
					//If there is an error with the model
					if (!$model->save()){
						$errorText = "";
						foreach ($model->errors as $err) {
							$errorText .= (isset($err[0]) ? $err[0] : '') . PHP_EOL;
						}
						$error = ["line" => "Error on line " . $count, "errors" => $errorText];
						array_push($result["message"], $error);
					}
					else{
						array_push($id, $model->id);
					}
				}

				//In order to sync directory, delete all directory that not exists in current upload file.
				\app\models\Directory::deleteAll(['not in', 'id', $id]);
			}

			if (count($result["message"])){
				$result["status"] = false;
				$transaction->rollBack();
			}else{
				$transaction->commit();
				$result["status"] = true;
			}

			return $result;
		} else {
		    echo 'No files';
		}
    }
}
