<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directory".
 *
 * @property integer $id
 * @property string $shop
 * @property string $location
 */
class Directory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'directory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shop', 'location'], 'required'],
            [['shop'], 'string', 'max' => 150],
            [['location'], 'string', 'max' => 50],
            [['location'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shop' => 'Shop',
            'location' => 'Location',
        ];
    }
}
