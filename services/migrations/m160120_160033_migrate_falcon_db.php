<?php

use yii\db\Schema;
use yii\db\Migration;

class m160120_160033_migrate_falcon_db extends Migration
{
    public function up()
    {
        $options = null;
        if ($this->db->driverName === 'mysql') {
            $options = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%directory}}', [
            'id' => 'INT PRIMARY KEY AUTO_INCREMENT',
            'shop' => 'VARCHAR(150) NOT NULL',
            'location' => 'VARCHAR(50) NOT NULL UNIQUE',
        ], $options);
    }

    public function down()
    {
        $this->dropTable('{{%directory}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
