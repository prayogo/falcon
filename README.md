Falcon Test Challenge
===============================

Create a directory listing for a new shopping mall which allows to keep all shops name and location (Floor & Shop Lot Number). It has one page which allows to add, edit and remove via ajax. it also allows to download the directory as a text file (e.g. tab-separated) and also upload a new version.
 
When it uploads a file, it updates the directory, creating, updating and deleting records as appropriate. In short it has to sync (not just replace) the web-added data with the uploaded file.
 
You are allowed to use any frameworks that you familiar such as jQuery, bootstrap, laravel or others. We encourage you to use newest, stable versions of whatever you will use to solve this challenge. 
Please understand that this test becomes the showcase of your skills, so don't skip on professional development standards.



FRAMEWORKS
-------------------
In this project, I'm using `Yii2` for the web services + `AngularJS` for the client


INSTALLING BOWER ASSETS
----------------------------
In this project, I'm using `bower` to manage the assets for client. To install run command on project folder: 
~~~
bower install
~~~


INSTALLING Yii2 VENDOR
---------------------------
To install `Yii2` vendor assets and also based on `Yii2` documentation, we have to run following command on the services folder:
~~~
composer global require "fxp/composer-asset-plugin:~1.1.1"
composer update
~~~


DATABASE
--------------------------
This project using `MySQL` database. We'll use database named `falcon_db`. Or you can change the database configuration setting at `services/config/db.php`.


DATABASE MIGRATION
--------------------------
This project using `Yii2 Migration`. To tell `Yii` to create the table, we need to run the migration under services folder:
~~~
yii migrate/up
~~~


Once everything above has been done, you can run or access the project through your apache server.

Please don't hesitate to contact me should you need further clarification. Thank you.